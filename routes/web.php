<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/boutique', 'ProduitController@index')->name('produits.index');
Route::get('/boutique/{slug}', 'ProduitController@show')->name('produits.show');

Route::get('/panier', 'Cartcontroller@index')->name('cart.index');
Route::post('/panier/ajouter', 'CartController@store')->name('cart.store');
Route::patch('/panier/{rowId}', 'CartController@update')->name('cart.update');
Route::delete('/panier/{rowId}', 'CartController@destroy')->name('cart.destroy');

//Route::get('/videpanier', function () {
//    Cart::destroy();
//});

Route::get('/paiement', 'CaisseController@index')->name('caisse.index');

//Route::post('/paiement', 'CaisseController@store')->name('caisse.store');
