<?php

namespace App\Http\Controllers;

use App\Produit;
use Illuminate\Http\Request;

class ProduitController extends Controller
{
    public function index()
    {
        $produits = Produit::inRandomOrder()->take(6)->get();

        return view('produits.index')->with('produits', $produits);
    }
    public function show($slug)
    {
        $produit = Produit::where('slug', $slug)->first();

        return view('produits.show')->with('produit', $produit);
    }
}
